This crate has been renamed and moved to the
[`codas`](https://crates.io/crates/codas) crate.

If this crate is not updated by June 21st, 2025, please
feel free to contact [`hello@alicorn.systems`] about
transferring the ownership (and/or purpose of) this crate.

## License

Copyright 2024 Alicorn Systems, Inc.

Licensed under the GNU Affero General Public License version 3,
as published by the Free Software Foundation. Refer to 
[the license file](../LICENSE.txt) for more information.

If you have any questions, please reach out to [`hello@alicorn.systems`].