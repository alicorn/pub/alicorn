Each sub-directory in this directory contains a crate
that was previously published to crate.io, and is no
longer under active development.

These crates, and their last published versions, were:

- `pact` version `0.4.0`
- `pact-crypto` version `0.4.0`
- `pact-derive` version `0.4.0`