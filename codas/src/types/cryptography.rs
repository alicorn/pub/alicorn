/// Cryptographic data types, like hashes and signatures.
///
/// # Unstable
///
/// These types may be split out into a separate crate in the future.
use ed25519_dalek::{Signature, Signer, SigningKey, VerifyingKey};
use rand_core::OsRng;
use snafu::Snafu;

use crate::{
    codec::{CodecError, Decodable, Encodable, Format, WritesEncodable},
    sized_byte_array,
    stream::Writes,
};

use super::Coda;

sized_byte_array!(
    /// Byte array containing a Blake3 hash.
    HashBytes,
    32
);

sized_byte_array!(
    /// Byte array containing an Ed25519 private key.
    PrivateKeyBytes,
    32
);

sized_byte_array!(
    /// Byte array containing an Ed25519 public key.
    PublicKeyBytes,
    32
);

sized_byte_array!(
    /// Byte array containing an Ed25519 signature.
    SignatureBytes,
    64
);

/// A hasher which creates [`HashBytes`].
#[derive(Default)]
pub struct CryptoHasher {
    hasher: blake3::Hasher,
}

impl CryptoHasher {
    /// Writes `bytes` to the in-progress hash.
    pub fn write(&mut self, bytes: &[u8]) {
        self.hasher.update(bytes);
    }

    /// Completes the hash and consumes `self`, returning it as [HashBytes].
    pub fn finalize(self) -> HashBytes {
        HashBytes::from(*self.hasher.finalize().as_bytes())
    }

    /// Completes the hash and consumes `self`, writing it into `bytes`.
    pub fn finalize_into_bytes(self, bytes: &mut HashBytes) {
        bytes.0 = *self.hasher.finalize().as_bytes();
    }
}

impl Writes for CryptoHasher {
    fn write(&mut self, buf: &[u8]) -> Result<usize, crate::stream::StreamError> {
        self.hasher.update(buf);
        Ok(buf.len())
    }

    fn write_all(&mut self, buf: &[u8]) -> Result<(), crate::stream::StreamError> {
        self.hasher.update(buf);
        Ok(())
    }
}

/// Signing (private) and verifying (public)
/// key pair which can create and verify
/// [`SignatureBytes`].
pub struct CryptoKeys {
    signer: CryptoSigner,
    verifier: CryptoVerifier,
}

impl CryptoKeys {
    /// Generates and returns a new pair of keys.
    pub fn generate() -> Self {
        let mut rng = OsRng;
        let signer = SigningKey::generate(&mut rng);
        let verifier = signer.verifying_key();
        CryptoKeys {
            signer: CryptoSigner {
                private_key: signer,
            },
            verifier: CryptoVerifier {
                public_key: verifier,
            },
        }
    }

    /// Tries to load a pair of keys from
    /// `private_key`.
    pub fn from_private(private_key: PrivateKeyBytes) -> Result<Self, CryptoError> {
        let signer = SigningKey::from_bytes(&private_key.0);
        let verifier = signer.verifying_key();
        Ok(CryptoKeys {
            signer: CryptoSigner {
                private_key: signer,
            },
            verifier: CryptoVerifier {
                public_key: verifier,
            },
        })
    }

    /// Consumes these keys, returning _only_
    /// their private key.
    pub fn into_private(self) -> PrivateKeyBytes {
        let mut bytes = PrivateKeyBytes::default();
        let private_key = &self.signer.private_key.to_keypair_bytes()[0..PrivateKeyBytes::SIZE];
        bytes.copy_from_slice(private_key);
        bytes
    }
}

/// Signing (private) key which
/// creates [`SignatureBytes`].
pub struct CryptoSigner {
    private_key: SigningKey,
}

/// Verifying (public) key which
/// verifies [`SignatureBytes`].
#[derive(Copy, Clone, Debug)]
pub struct CryptoVerifier {
    public_key: VerifyingKey,
}

impl TryFrom<&PublicKeyBytes> for CryptoVerifier {
    type Error = CryptoError;

    fn try_from(public_key: &PublicKeyBytes) -> Result<Self, Self::Error> {
        let public_key =
            VerifyingKey::from_bytes(&public_key.0).map_err(|_| CryptoError::InvalidPublicKey {
                pub_key: *public_key,
            })?;

        Ok(CryptoVerifier { public_key })
    }
}

/// A cryptographic certificate, containing
/// [`SignatureBytes`] accompanied by the
/// [`PublicKeyBytes`] of the entity that
/// created the signature.
#[derive(Copy, Clone, Debug, Default)]
pub struct CryptoCert {
    /// The public key of the entity
    /// that created [`Self::signature`].
    pub public_key: PublicKeyBytes,

    /// The signature.
    pub signature: SignatureBytes,
}

impl CryptoCert {
    /// Signs `data` with `signer`, replacing
    /// `self`'s current signature with the result.
    pub fn sign(
        &mut self,
        signer: &impl CryptoSigns,
        data: &[&[u8]],
    ) -> core::result::Result<(), CryptoError> {
        self.public_key = signer.public_key_bytes();
        self.signature = signer.sign(data)?;

        Ok(())
    }

    /// Returns `Ok` iff this certificate's
    /// signature is valid and matches `data`.
    pub fn verify(&self, data: &[&[u8]]) -> core::result::Result<(), CryptoError> {
        let key = CryptoVerifier::try_from(&self.public_key)?;
        key.verify(data, &self.signature)
    }
}

impl Eq for CryptoCert {}
impl PartialEq for CryptoCert {
    fn eq(&self, other: &Self) -> bool {
        self.public_key == other.public_key && self.signature == other.signature
    }
}

impl Ord for CryptoCert {
    fn cmp(&self, other: &Self) -> core::cmp::Ordering {
        self.signature.cmp(&other.signature)
    }
}

impl PartialOrd for CryptoCert {
    fn partial_cmp(&self, other: &Self) -> Option<core::cmp::Ordering> {
        Some(self.cmp(other))
    }
}

impl core::hash::Hash for CryptoCert {
    fn hash<H: core::hash::Hasher>(&self, state: &mut H) {
        self.public_key.hash(state);
        self.signature.hash(state);
    }
}

impl Encodable for CryptoCert {
    const FORMAT: Format = PublicKeyBytes::FORMAT.with(SignatureBytes::FORMAT);

    fn encode(&self, writer: &mut (impl WritesEncodable + ?Sized)) -> Result<(), CodecError> {
        writer.write_data(&self.public_key)?;
        writer.write_data(&self.signature)?;
        Ok(())
    }
}

impl Decodable for CryptoCert {
    fn decode(
        &mut self,
        reader: &mut (impl crate::codec::ReadsDecodable + ?Sized),
        header: Option<crate::codec::DataHeader>,
    ) -> Result<(), CodecError> {
        Self::ensure_no_header(header)?;
        reader.read_data_into(&mut self.public_key)?;
        reader.read_data_into(&mut self.signature)?;
        Ok(())
    }
}

/// A thing that can be represented as a cryptographic hash.
pub trait HasCryptoHash {
    /// Writes `self`'s cryptographhically hashable
    /// data to `hasher`.
    fn crypto_hash_into(&self, hasher: &mut CryptoHasher);

    /// Returns a new [`CryptoHasher`] containing `self`'s
    /// cryptographically hashable data.
    ///
    /// The hashable data is not guaranteed to contain the
    /// entirety of `self`'s data. For example, some data
    /// structures may contain a certificate that indirectly
    /// contains all of `self`'s data; in these cases,
    /// the hashable data returned by this method may be
    /// the raw bytes of the certificate's signature.
    fn crypto_hasher(&self) -> CryptoHasher {
        let mut hasher = CryptoHasher::default();
        self.crypto_hash_into(&mut hasher);
        hasher
    }
}

impl HasCryptoHash for CryptoCert {
    fn crypto_hash_into(&self, hasher: &mut CryptoHasher) {
        hasher.write(&self.signature);
    }
}

impl HasCryptoHash for Coda {
    fn crypto_hash_into(&self, hasher: &mut CryptoHasher) {
        let _ = self.encode(hasher);
    }
}

/// A thing that has associated [`PublicKeyBytes`].
pub trait HasCryptoPublicKey {
    /// Returns this thing's public key.
    fn public_key_bytes(&self) -> PublicKeyBytes;
}

impl HasCryptoPublicKey for CryptoKeys {
    fn public_key_bytes(&self) -> PublicKeyBytes {
        self.verifier.public_key_bytes()
    }
}

impl HasCryptoPublicKey for CryptoSigner {
    fn public_key_bytes(&self) -> PublicKeyBytes {
        (*self.private_key.verifying_key().as_bytes()).into()
    }
}

impl HasCryptoPublicKey for CryptoVerifier {
    fn public_key_bytes(&self) -> PublicKeyBytes {
        (*self.public_key.as_bytes()).into()
    }
}

/// A thing that creates [`SignatureBytes`].
pub trait CryptoSigns: HasCryptoPublicKey {
    /// Signs `message` with this signer's private key,
    /// returning `Ok(signature)` iff signing was successful.
    fn sign(&self, message: &[&[u8]]) -> Result<SignatureBytes, CryptoError>;
}

impl CryptoSigns for CryptoKeys {
    fn sign(&self, message: &[&[u8]]) -> Result<SignatureBytes, CryptoError> {
        self.signer.sign(message)
    }
}

impl CryptoSigns for CryptoSigner {
    fn sign(&self, message: &[&[u8]]) -> Result<SignatureBytes, CryptoError> {
        let signature = self
            .private_key
            .try_sign(message.concat().as_slice())
            .expect("signing failure");
        Ok(signature.to_bytes().into())
    }
}

/// A thing that verifies [`SignatureBytes`].
pub trait CryptoVerifies: HasCryptoPublicKey {
    /// Verifies `signature` against `message`,
    /// returning `Ok` iff the `signature` is
    /// valid and corresponds to this verifier's
    /// public key.
    fn verify(&self, message: &[&[u8]], signature: &SignatureBytes) -> Result<(), CryptoError>;
}

impl CryptoVerifies for CryptoKeys {
    fn verify(&self, message: &[&[u8]], signature: &SignatureBytes) -> Result<(), CryptoError> {
        self.verifier.verify(message, signature)
    }
}

impl CryptoVerifies for CryptoVerifier {
    fn verify(&self, message: &[&[u8]], signature: &SignatureBytes) -> Result<(), CryptoError> {
        let message = message.concat();
        let message = message.as_slice();
        let sig = Signature::from_bytes(&signature.0);
        self.public_key
            .verify_strict(message, &sig)
            .map_err(|_| CryptoError::InvalidSignature {
                signature: *signature,
            })
    }
}

/// An error that may occur when interacting with cryptographic data.
#[derive(Debug, Snafu, Clone)]
pub enum CryptoError {
    #[snafu(display("the private key could not be loaded as an Ed25519 private key"))]
    InvalidPrivateKey,

    #[snafu(display("{pub_key} could not be loaded as an Ed25519 public key"))]
    InvalidPublicKey { pub_key: PublicKeyBytes },

    #[snafu(display("{signature} was not a valid Ed25519 signature for the provided message"))]
    InvalidSignature { signature: SignatureBytes },
}
