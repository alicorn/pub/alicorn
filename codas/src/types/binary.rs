//! Binary data types (i.e., bytes).
//!
//! ## Text Encoding and Decoding
//!
//! This module provides utilities for encoding
//! and decoding slices of bytes as hexadecimal
//! text.
use core::fmt::{Debug, Formatter, Write};

use snafu::Snafu;

use crate::{
    codec::{
        CodecError, DataHeader, Decodable, Encodable, Format, FormatMetadata, ReadsDecodable,
        WritesEncodable,
    },
    types::Text,
};

// Fixed-size `[u8; SIZE]` codec.
impl<const SIZE: usize> Encodable for [u8; SIZE] {
    /// Encoded as a [`Format::Data`] containing a
    /// [`Format::Blob(SIZE)`](Format::Blob).
    const FORMAT: Format = Format::data(0).with(Format::Blob(SIZE as FormatMetadata));

    fn encode(&self, writer: &mut (impl WritesEncodable + ?Sized)) -> Result<(), CodecError> {
        writer.write_all(self)?;
        Ok(())
    }
}

impl<const SIZE: usize> Decodable for [u8; SIZE] {
    fn decode(
        &mut self,
        reader: &mut (impl ReadsDecodable + ?Sized),
        header: Option<DataHeader>,
    ) -> Result<(), CodecError> {
        let _ = Self::ensure_header(header, &[0])?;
        reader.read_exact(self)?;
        Ok(())
    }
}

/// Macro which generates a [new type](https://doc.rust-lang.org/rust-by-example/generics/new_types.html)
/// a struct wrapping a fixed-size `[u8]` array,
/// enabling sype-safe sharing.
#[macro_export]
macro_rules! sized_byte_array {
    (
        // Optional type metadata (e.g., docs).
        $(#[$meta:meta])*

        // Type name of the array
        $type_name:ident,

        // Fixed size of the array
        $array_size:expr
    ) => {

        $(#[$meta])*
        #[repr(transparent)]
        #[derive(Copy, Clone)]
        pub struct $type_name([core::primitive::u8; $array_size]);

        impl $type_name {
            /// Size of this type, in bytes.
            pub const SIZE: usize = $array_size;

            /// Null ("empty") bytes initialized to `0`.
            pub const NULL: Self = Self([0; $array_size]);

            /// Decodes a `hex` string into these bytes.
            pub fn from_hex(&mut self, hex: &str) -> Result<(), $crate::types::binary::BinaryError> {
                $crate::types::binary::fixed_bytes_from_hex(hex, &mut self.0)
            }

            /// Encodes a hex string from these bytes.
            pub fn to_hex(&self) -> $crate::types::Text {
                $crate::types::binary::hex_from_bytes(&self.0)
            }
        }

        impl core::default::Default for $type_name {
            fn default() -> Self {
                Self::NULL
            }
        }

        impl core::convert::From<[core::primitive::u8; $array_size]> for $type_name {
            fn from(bytes: [core::primitive::u8; $array_size]) -> Self {
                $type_name(bytes)
            }
        }

        impl core::convert::From<$type_name> for [core::primitive::u8; $array_size] {
            fn from(bytes: $type_name) -> Self {
                bytes.0
            }
        }

        impl core::borrow::Borrow<[core::primitive::u8; $array_size]> for $type_name {
            fn borrow(&self) -> &[core::primitive::u8; $array_size] {
                &self.0
            }
        }

        impl core::borrow::BorrowMut<[core::primitive::u8; $array_size]> for $type_name {
            fn borrow_mut(&mut self) -> &mut [core::primitive::u8; $array_size] {
                &mut self.0
            }
        }

        impl core::ops::Deref for $type_name {
            type Target = [core::primitive::u8];

            fn deref(&self) -> &Self::Target {
                &self.0
            }
        }

        impl core::ops::DerefMut for $type_name {
            fn deref_mut(&mut self) -> &mut Self::Target {
                &mut self.0
            }
        }

        impl core::cmp::Eq for $type_name {}
        impl core::cmp::PartialEq for $type_name {
            fn eq(&self, other: &Self) -> core::primitive::bool {
                self.0 == other.0
            }
        }

        impl core::cmp::Ord for $type_name {
            fn cmp(&self, other: &Self) -> core::cmp::Ordering {
                self.0.cmp(&other.0)
            }
        }

        impl core::cmp::PartialOrd for $type_name {
            fn partial_cmp(&self, other: &Self) -> core::option::Option<core::cmp::Ordering> {
                Some(self.cmp(other))
            }
        }

        impl core::hash::Hash for $type_name {
            fn hash<H: core::hash::Hasher>(&self, state: &mut H) {
                self.0.hash(state);
            }
        }

        impl core::fmt::Display for $type_name {
            fn fmt(&self, f: &mut core::fmt::Formatter<'_>) -> core::fmt::Result {
                $crate::types::binary::format_bytes_as_hex(f, &self.0)
            }
        }

        impl core::fmt::Debug for $type_name {
            fn fmt(&self, f: &mut core::fmt::Formatter<'_>) -> core::fmt::Result {
                $crate::types::binary::format_bytes_as_hex(f, &self.0)
            }
        }

        impl $crate::codec::Encodable for $type_name {
            const FORMAT: $crate::codec::Format = <[core::primitive::u8; $array_size]>::FORMAT;

            fn encode(&self, writer: &mut (impl $crate::codec::WritesEncodable + ?Sized)) -> Result<(), $crate::codec::CodecError> {
                self.0.encode(writer)
            }
        }

        impl $crate::codec::Decodable for $type_name {
            fn decode(
                &mut self,
                reader: &mut (impl $crate::codec::ReadsDecodable + ?Sized),
                header: Option<$crate::codec::DataHeader>,
            ) -> Result<(), $crate::codec::CodecError> {
                self.0.decode(reader, header)
            }
        }
    };
}

/// Decodes a vector of bytes from a `hex` into `bytes`.
///
/// If an error is returned, the contents of `bytes` are undefined.
pub fn fixed_bytes_from_hex<const SIZE: usize>(
    hex: &str,
    bytes: &mut [u8; SIZE],
) -> Result<(), BinaryError> {
    let length = hex.len();
    if length % 2 != 0 {
        return Err(BinaryError::UnevenHex { actual: length });
    } else if length / 2 != SIZE {
        return Err(BinaryError::WrongHexLength {
            expected: length / 2,
            actual: length,
        });
    }

    let mut hex_bytes = hex.as_bytes().iter();
    let mut i = 0;
    while let (Some(h), Some(l)) = (hex_bytes.next(), hex_bytes.next()) {
        let h = match h {
            b'0'..=b'9' => h - b'0',
            b'a'..=b'f' => h - b'a' + 10,
            b'A'..=b'F' => h - b'A' + 10,
            character => {
                return Err(BinaryError::UnexpectedHexCharacter {
                    character: *character,
                })
            }
        };

        let l = match l {
            b'0'..=b'9' => l - b'0',
            b'a'..=b'f' => l - b'a' + 10,
            b'A'..=b'F' => l - b'A' + 10,
            character => {
                return Err(BinaryError::UnexpectedHexCharacter {
                    character: *character,
                })
            }
        };

        bytes[i] = (h << 4) | l;
        i += 1;
    }

    Ok(())
}

/// Decodes a vector of bytes from a `hex`.
pub fn bytes_from_hex(hex: &str) -> Result<alloc::vec::Vec<u8>, BinaryError> {
    let length = hex.len();
    if length % 2 != 0 {
        return Err(BinaryError::UnevenHex { actual: length });
    }

    let mut hex_bytes = hex.as_bytes().iter();
    let mut bytes = alloc::vec::Vec::with_capacity(length / 2);
    while let (Some(h), Some(l)) = (hex_bytes.next(), hex_bytes.next()) {
        let h = match h {
            b'0'..=b'9' => h - b'0',
            b'a'..=b'f' => h - b'a' + 10,
            b'A'..=b'F' => h - b'A' + 10,
            character => {
                return Err(BinaryError::UnexpectedHexCharacter {
                    character: *character,
                })
            }
        };

        let l = match l {
            b'0'..=b'9' => l - b'0',
            b'a'..=b'f' => l - b'a' + 10,
            b'A'..=b'F' => l - b'A' + 10,
            character => {
                return Err(BinaryError::UnexpectedHexCharacter {
                    character: *character,
                })
            }
        };

        bytes.push((h << 4) | l)
    }

    Ok(bytes)
}

/// Lookup table for hexadecimal character codes.
static HEX_LUT: [u8; 16] = [
    b'0', b'1', b'2', b'3', b'4', b'5', b'6', b'7', b'8', b'9', b'a', b'b', b'c', b'd', b'e', b'f',
];

/// Returns a lowercase hexadecimal string encoded from `bytes`.
pub fn hex_from_bytes(bytes: &[u8]) -> Text {
    let mut text = alloc::string::String::with_capacity(bytes.len() * 2);
    for byte in bytes {
        text.push(HEX_LUT[(byte >> 4) as usize] as char);
        text.push(HEX_LUT[(byte & 0xF) as usize] as char);
    }

    text.into()
}

/// Encodes `bytes` to `fmt` as a lowercase hexadecimal string.
pub fn format_bytes_as_hex(fmt: &mut Formatter, bytes: &[u8]) -> Result<(), core::fmt::Error> {
    for byte in bytes {
        fmt.write_char(HEX_LUT[(byte >> 4) as usize] as char)?;
        fmt.write_char(HEX_LUT[(byte & 0xF) as usize] as char)?;
    }

    Ok(())
}

#[derive(Snafu, Debug)]
pub enum BinaryError {
    #[snafu(display(
        "expected a hexadecimal string containing an even number of bytes, not {actual} byte(s)"
    ))]
    UnevenHex { actual: usize },

    #[snafu(display(
        "expected a hexadecimal string containing {expected} bytes, not {actual} byte(s)"
    ))]
    WrongHexLength { expected: usize, actual: usize },

    #[snafu(display("hexadecimal string contained an unexpected character code: {character}"))]
    UnexpectedHexCharacter { character: u8 },
}

#[cfg(test)]
mod test {
    use crate::codec::{ReadsDecodable, WritesEncodable};

    use super::*;

    #[test]
    fn test_sized_byte_array_codec() {
        sized_byte_array!(TestArray, 9);
        let value: TestArray = TestArray([1, 2, 3, 4, 5, 6, 7, 8, 9]);
        let mut encoded = vec![];
        encoded.write_data(&value).expect("encoded");
        let mut decoded = TestArray::NULL;
        encoded
            .as_slice()
            .read_data_into(&mut decoded)
            .expect("decoded");
        assert_eq!(value, decoded);
    }

    #[test]
    fn test_u8_array_codec() {
        let value: [u8; 8] = [1, 2, 3, 4, 5, 6, 7, 8];
        let mut encoded = vec![];
        encoded.write_data(&value).expect("encoded");
        let mut decoded = [0u8; 8];
        encoded
            .as_slice()
            .read_data_into(&mut decoded)
            .expect("decoded");
        assert_eq!(value, decoded);
    }

    #[test]
    fn test_hex() {
        // "Ferrous" in hexadecimal-encoded UTF-8 encoding.
        const TEST_STR: &str = "Ferrous";
        const TEST_HEX_UTF8_STR: &str = "466572726f7573";

        // Decode hexadecimal dynamic vector.
        let bytes = bytes_from_hex(TEST_HEX_UTF8_STR).unwrap();
        assert_eq!(TEST_STR.as_bytes(), bytes);

        // Decode hexadecimal into a fixed-length array.
        let mut bytes = [0u8; 7];
        assert!(fixed_bytes_from_hex(TEST_HEX_UTF8_STR, &mut bytes).is_ok());
        assert_eq!(TEST_STR.as_bytes(), bytes);

        // Encode bytes into hexadecimal.
        let hex = hex_from_bytes(TEST_STR.as_bytes());
        assert_eq!(TEST_HEX_UTF8_STR, hex);

        // Write bytes into hexadecimal via a formatter.
        sized_byte_array!(TestBytes, 7);
        let mut bytes = TestBytes::NULL;
        assert!(bytes.from_hex(TEST_HEX_UTF8_STR).is_ok());
        assert_eq!(TEST_STR.as_bytes(), bytes.as_ref());
        let hex = bytes.to_string();
        assert_eq!(TEST_HEX_UTF8_STR, hex);
    }
}
