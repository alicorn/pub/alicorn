Refer to [How We Make This](#how-we-make-this) for info on what
this file is and how it's structured.

## Unreleased

- Initial release (`1.0.0`).

## How We Make This

> Our guidelines follow ["Keep a Changelog"](https://keepachangelog.com/en/1.0.0/).

This file contains a chronological list of all major changes
made to this workspace. "Major" means the addition, removal,
or modification of any user-facing ("public") API.

Changelog entries start with a single word that, at a glance,
describes the nature of the change:

- `Added`, `Changed`, `Replaced`, `Removed`: 
  For the addition, removal, modification, renaming and/or
  replacement of functionality.

- `Fixed`: For changes that fix unintended behavior in our code,
  including security vulnerabilities. The rationale for not
  having `Security` is that, in many cases, unintended behavior
  _is_ potentially exploitable as a security vulnerability, and
  we'd rather not argue semantics in the changelog.

The changelog is divided into sections per release, with each
section having the followng structure:

1. A header `##` containing just the release tag/version (e.g., `0.1`),
   followed by an optional summary of the changes in the release.

2. For each crate, a header `###` containing just the crate name (e.g., `codas`),
   followed by:
   
    - An optional summary of the changes in the release.

    - All changelogs entries for that crate.

If any change in any section is a _breaking_ change, the
entire section is labeled with `- Breaking Changes`.

We maintain an `Unreleased` section containing all changes
made to the workspace since the last release that was tagged
and published (e.g., to Crates.io).