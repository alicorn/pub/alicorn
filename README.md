A toolbox for building distributed systems.

## What's Here (Crates!)

This repository is a [Cargo Workspace](https://doc.rust-lang.org/book/ch14-03-cargo-workspaces.html)
of several closely related crates:

- [`codas`](codas/): Compact and streamable data format that works anywhere--from web apps to robots.

- [`codas-macros`](codas-macros/): Macros for generating Rust data structures for codas.

- [`codas-flow`](codas-flow/): Low-latency, high-throughput bounded queues (\"data flows\") for (a)synchronous and event-driven systems.

Refer to individual crates' READMEs for more detailed info.

## License

Copyright 2025 Alicorn Systems, Inc.

Licensed under the GNU Affero General Public License version 3,
as published by the Free Software Foundation. Refer to 
[the license file](LICENSE.txt) for more information.

If you have any questions, please reach out to [`hello@alicorn.systems`].